CREATE TABLE expense_data (
    ed_id int(11) NOT NULL AUTO_INCREMENT,
    ed_user_id int(11) NOT NULL,
    ed_category int(11),
    ed_amount double(18,2),
    ed_type int(11),
    ed_note text,
    ed_spent_at date,
    ed_created_at datetime DEFAULT CURRENT_TIMESTAMP,
    ed_created_by int(11),
    ed_updated_at datetime,
    ed_updated_by int(11),
    PRIMARY KEY(ed_id)
);

CREATE TABLE mr_global_param(
    mgp_id int(11) NOT NULL AUTO_INCREMENT,
    mgp_slug varchar(255),
    mgp_code_id int(11),
    mgp_description varchar(255),
    PRIMARY KEY(mgp_id)
);

INSERT INTO mr_global_param(mgp_slug, mgp_code_id, mgp_description) VALUES
('expense_category', 1, 'Food'),
('expense_category', 2, 'Transportation'),
('expense_category', 3, 'Entertainment'),
('expense_category', 4, 'Savings'),
('expense_category', 5, 'Healthcare'),
('expense_category', 6, 'Utilities'),
('expense_category', 7, 'Personal Care'),
('expense_category', 8, 'Grocery'),
('expense_category', 9, 'Housing'),
('expense_category', 10, 'Travel'),
('expense_category', 11, 'Debts'),
('expense_category', 12, 'Miscellaneous'),
('expense_type', 1, 'Cash'),
('expense_type', 2, 'Bank Transfer'),
('expense_type', 3, 'Debit Card'),
('expense_type', 4, 'Credit Card'),
('expense_type', 5, 'eWallet'),
('expense_type', 6, 'QRIS');