package expensetrackerexpenseservice.restful.service;

import expensetrackerexpenseservice.restful.entity.ExpenseData;
import expensetrackerexpenseservice.restful.model.data.DefaultData;
import expensetrackerexpenseservice.restful.model.data.query.MostExpenseProjection;
import expensetrackerexpenseservice.restful.model.request.CreateExpenseDataRequest;
import expensetrackerexpenseservice.restful.model.request.ExpenseQueryParams;
import expensetrackerexpenseservice.restful.model.request.UpdateExpenseDataRequest;
import expensetrackerexpenseservice.restful.repository.ExpenseDataRepository;
import jakarta.validation.Validator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@SpringBootTest
class ExpenseDataServiceImplTest {
    @InjectMocks
    private ExpenseDataServiceImpl expenseDataService;

    @Mock
    private ExpenseDataRepository expenseDataRepository;

    @Mock
    private ExpenseDetailDataBuilder expenseDetailDataBuilder;

    @Mock
    private GlobalParamService globalParamService;

    @Mock
    private Validator validator;

    private CreateExpenseDataRequest createRequest = CreateExpenseDataRequest
            .builder()
            .category(DefaultData.builder().id(1L).build())
            .type(DefaultData.builder().id(1L).build())
            .build();
    private UpdateExpenseDataRequest updateRequest = UpdateExpenseDataRequest
            .builder()
            .category(DefaultData.builder().id(1L).build())
            .type(DefaultData.builder().id(1L).build())
            .build();
    private ExpenseQueryParams queryParams = ExpenseQueryParams.builder()
            .page(1)
            .limit(10)
            .build();
    private MostExpenseProjection implMostExpenseProjection = new MostExpenseProjection() {
        @Override
        public Integer getData() {
            return 1;
        }

        @Override
        public Double getTotalSpent() {
            return 20000.0;
        }

        @Override
        public Integer getDataCount() {
            return 2;
        }
    };

    private Long userId = 1L;
    private Long expenseId = 1L;

    @Test
    public void createExpenseData_shouldCall_validatorService_ExpenseDataRepository_andBuildExpenseDetailData() {
        expenseDataService.createExpense(createRequest, userId);

        Mockito.verify(validator, Mockito.times(1)).validate(any());
        Mockito.verify(expenseDataRepository, Mockito.times(1)).save(any());
        Mockito.verify(expenseDetailDataBuilder, Mockito.times(1)).buildExpenseDetailData(any());
    }

    @Test
    public void updateExpenseData_whenExpenseDataDoesNotExists_thenThrow() {
        Throwable exception = Assertions.assertThrows(
                ResponseStatusException.class, () -> expenseDataService.updateExpense(updateRequest, expenseId, userId)
        );

        Assertions.assertEquals(HttpStatus.NOT_FOUND+" \"Expense not found\"", exception.getMessage());
    }

    @Test
    public void updateExpenseData_shouldCall_validatorService_ExpenseDataRepository_andBuildExpenseDetailData() {
        when(expenseDataRepository.findById(any())).thenReturn(
                Optional.of(new ExpenseData())
        );

        expenseDataService.updateExpense(updateRequest, expenseId, userId);

        Mockito.verify(validator, Mockito.times(1)).validate(any());
        Mockito.verify(expenseDataRepository, Mockito.times(1)).save(any());
        Mockito.verify(expenseDetailDataBuilder, Mockito.times(1)).buildExpenseDetailData(any());
    }

    @Test
    public void deleteExpenseData_shouldCall_deleteFromExpenseDataRepository() {
        expenseDataService.deleteExpense(expenseId);

        Mockito.verify(expenseDataRepository, Mockito.times(1)).deleteById(expenseId);
    }

    @Test
    public void getAllExpense_shouldCall_getAllExpenseWithFilter_fromExpenseDataRepository() {
        when(expenseDataRepository.getAllExpensesWithFilter(any(), any(), any(), any(), any(), any())).thenReturn(
                Page.empty()
        );

        expenseDataService.getAllExpense(queryParams, userId);

        Mockito.verify(expenseDataRepository, Mockito.times(1)).getAllExpensesWithFilter(
                any(), any(), any(), any(), any(), any()
        );
    }

    @Test
    public void getMonthlyReport_shouldCall_listOfExpenseDataRepositoryQuery() {
        when(expenseDataRepository.getMostExpenseCategory(any(), any(), any())).thenReturn(
                implMostExpenseProjection
        );
        when(expenseDataRepository.getMostUsedCategory(any(), any(), any())).thenReturn(
                implMostExpenseProjection
        );
        when(expenseDataRepository.getMostExpenseMethod(any(), any(), any())).thenReturn(
                implMostExpenseProjection
        );
        when(expenseDataRepository.getMostUsedMethod(any(), any(), any())).thenReturn(
                implMostExpenseProjection
        );

        expenseDataService.getMonthlyReport(1, 2024, userId);

        Mockito.verify(expenseDataRepository, times(1)).getMonthlyExpenses(any(), any(), any());
        Mockito.verify(expenseDataRepository, times(1)).getMostExpenseCategory(any(), any(), any());
        Mockito.verify(expenseDataRepository, times(1)).getMostUsedCategory(any(), any(), any());
        Mockito.verify(expenseDataRepository, times(1)).getMostExpenseMethod(any(), any(), any());
        Mockito.verify(expenseDataRepository, times(1)).getMostUsedMethod(any(), any(), any());
    }
}