package expensetrackerexpenseservice.restful.constant;

public class Constant {
    public static final class GlobalParamSlug {
        public static final String EXPENSE_CATEGORY = "expense_category";
        public static final String EXPENSE_TYPE = "expense_type";
    }
}
