package expensetrackerexpenseservice.restful.service;

import expensetrackerexpenseservice.restful.constant.Constant;
import expensetrackerexpenseservice.restful.entity.ExpenseData;
import expensetrackerexpenseservice.restful.model.data.DefaultData;
import expensetrackerexpenseservice.restful.model.data.ExpenseDetailData;
import expensetrackerexpenseservice.restful.model.data.MostExpenseData;
import expensetrackerexpenseservice.restful.model.data.query.MostExpenseProjection;
import expensetrackerexpenseservice.restful.model.request.CreateExpenseDataRequest;
import expensetrackerexpenseservice.restful.model.request.ExpenseQueryParams;
import expensetrackerexpenseservice.restful.model.request.UpdateExpenseDataRequest;
import expensetrackerexpenseservice.restful.model.response.MonthlyExpenseReportResponse;
import expensetrackerexpenseservice.restful.model.response.PaginationBaseResponse;
import expensetrackerexpenseservice.restful.repository.ExpenseDataRepository;
import jakarta.transaction.Transactional;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.text.DecimalFormat;
import java.time.Month;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

@Service
public class ExpenseDataServiceImpl implements ExpenseDataService {
    @Autowired
    public ExpenseDataRepository expenseDataRepository;

    @Autowired
    public ExpenseDetailDataBuilder expenseDetailDataBuilder;

    @Autowired
    public GlobalParamService globalParamService;

    @Autowired
    public Validator validator;

    @Override
    @Transactional(rollbackOn = Exception.class)
    public ExpenseDetailData createExpense(CreateExpenseDataRequest request, Long actorId) {
        validateRequest(request);

        ExpenseData expenseData = new ExpenseData();
        expenseData.setUserId(actorId);
        expenseData.setAmount(request.getAmount());
        expenseData.setCategory(request.getCategory().getId());
        expenseData.setType(request.getType().getId());
        expenseData.setNote(request.getNote());
        expenseData.setSpentAt(request.getSpentAt());
        expenseData.setCreatedBy(actorId);

        ExpenseData savedExpense = expenseDataRepository.save(expenseData);

        return expenseDetailDataBuilder.buildExpenseDetailData(savedExpense);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public ExpenseDetailData updateExpense(UpdateExpenseDataRequest request, Long expenseId, Long actorId) {
        validateRequest(request);

        ExpenseData expenseData = expenseDataRepository.findById(expenseId).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "Expense not found")
        );
        expenseData.setAmount(request.getAmount());
        expenseData.setCategory(request.getCategory().getId());
        expenseData.setType(request.getType().getId());
        expenseData.setNote(request.getNote());
        expenseData.setSpentAt(request.getSpentAt());
        expenseData.setUpdatedBy(actorId);

        ExpenseData savedExpense = expenseDataRepository.save(expenseData);

        return expenseDetailDataBuilder.buildExpenseDetailData(savedExpense);
    }

    @Override
    public PaginationBaseResponse<ExpenseDetailData> getAllExpense(ExpenseQueryParams params, Long userId) {
        Page<ExpenseData> pagingExpenseData = expenseDataRepository.getAllExpensesWithFilter(
                userId,
                params.getCategory(),
                params.getType(),
                params.getStartDate(),
                params.getEndDate(),
                PageRequest.of((params.getPage() - 1), params.getLimit())
        );

        Stream<ExpenseData> expensesStream = pagingExpenseData.getContent().stream();
        List<ExpenseDetailData> expenseDetailList = expensesStream.map(expenseData -> {
                    return expenseDetailDataBuilder.buildExpenseDetailData(expenseData);
                }
        ).collect(Collectors.toList());

        return PaginationBaseResponse.<ExpenseDetailData>builder()
                .currentPage(params.getPage())
                .totalPage(pagingExpenseData.getTotalPages())
                .totalData(pagingExpenseData.getTotalElements())
                .pagingData(expenseDetailList)
                .build();
    }

    @Override
    public MonthlyExpenseReportResponse getMonthlyReport(Integer month, Integer year, Long userId) {
        List<ExpenseData> monthlyExpenses = expenseDataRepository.getMonthlyExpenses(userId, month, year);
        Stream<ExpenseData> monthlyExpensesStream = monthlyExpenses.stream();
        double[] expensesList = monthlyExpensesStream.mapToDouble(expense -> expense.getAmount()).toArray();

        double totalSpent = DoubleStream.of(expensesList).sum();

        MostExpenseProjection mostExpenseCategory = expenseDataRepository.getMostExpenseCategory(
                userId, month, year
        );
        DefaultData mostExpenseCategoryData = getDefaultData(
                Constant.GlobalParamSlug.EXPENSE_CATEGORY, mostExpenseCategory.getData()
        );

        MostExpenseProjection mostUsedCategory = expenseDataRepository.getMostUsedCategory(
                userId, month, year
        );
        DefaultData mostUsedCategoryData = getDefaultData(
                Constant.GlobalParamSlug.EXPENSE_CATEGORY, mostUsedCategory.getData()
        );

        MostExpenseProjection mostExpenseMethod = expenseDataRepository.getMostExpenseMethod(
                userId, month, year
        );
        DefaultData mostExpenseMethodData = getDefaultData(
                Constant.GlobalParamSlug.EXPENSE_TYPE, mostExpenseMethod.getData()
        );

        MostExpenseProjection mostUsedMethod = expenseDataRepository.getMostUsedMethod(
                userId, month, year
        );
        DefaultData mostUsedMethodData = getDefaultData(
                Constant.GlobalParamSlug.EXPENSE_TYPE, mostUsedMethod.getData()
        );

        return MonthlyExpenseReportResponse.builder()
                .month(
                        DefaultData.builder()
                                .id(month.longValue())
                                .name(Month.of(month).toString())
                                .build()
                )
                .year(year)
                .totalExpense(formattedDecimal(totalSpent))
                .mostUsedCategory(
                        MostExpenseData.builder()
                                .totalAmount(formattedDecimal(mostUsedCategory.getTotalSpent()))
                                .count(mostUsedCategory.getDataCount())
                                .data(mostUsedCategoryData)
                                .build()
                )
                .mostExpenseCategory(
                        MostExpenseData.builder()
                                .totalAmount(formattedDecimal(mostExpenseCategory.getTotalSpent()))
                                .count(mostExpenseCategory.getDataCount())
                                .data(mostExpenseCategoryData)
                                .build()
                )
                .mostUsedMethod(
                        MostExpenseData.builder()
                                .totalAmount(formattedDecimal(mostUsedMethod.getTotalSpent()))
                                .count(mostUsedMethod.getDataCount())
                                .data(mostUsedMethodData)
                                .build()
                )
                .mostExpenseMethod(
                        MostExpenseData.builder()
                                .totalAmount(formattedDecimal(mostExpenseMethod.getTotalSpent()))
                                .count(mostExpenseMethod.getDataCount())
                                .data(mostExpenseMethodData)
                                .build()
                )
                .build();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void deleteExpense(Long expenseId) {
        expenseDataRepository.deleteById(expenseId);
    }

    private void validateRequest(Object request) {
        Set<ConstraintViolation<Object>> validation = validator.validate(request);
        if (!validation.isEmpty()) {
            throw new ConstraintViolationException(validation);
        }
    }

    private Double formattedDecimal(Double amount) {
        DecimalFormat decimalFormat = new DecimalFormat("#.##");

        return Double.valueOf(decimalFormat.format(amount));
    }

    private DefaultData getDefaultData(String slug, Integer dataId) {
        return globalParamService.getDefaultDataBySlugAndCodeId(slug, dataId.longValue());
    }
}
