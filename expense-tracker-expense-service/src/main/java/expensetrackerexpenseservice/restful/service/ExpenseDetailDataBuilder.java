package expensetrackerexpenseservice.restful.service;

import expensetrackerexpenseservice.restful.constant.Constant;
import expensetrackerexpenseservice.restful.entity.ExpenseData;
import expensetrackerexpenseservice.restful.model.data.DefaultData;
import expensetrackerexpenseservice.restful.model.data.ExpenseDetailData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExpenseDetailDataBuilder {
    @Autowired
    private GlobalParamService globalParamService;

    public ExpenseDetailData buildExpenseDetailData(ExpenseData expenseData) {
        DefaultData category = globalParamService.getDefaultDataBySlugAndCodeId(
                Constant.GlobalParamSlug.EXPENSE_CATEGORY,
                expenseData.getCategory()
        );
        DefaultData type = globalParamService.getDefaultDataBySlugAndCodeId(
                Constant.GlobalParamSlug.EXPENSE_TYPE,
                expenseData.getType()
        );

        return ExpenseDetailData.builder()
                .id(expenseData.getId())
                .amount(expenseData.getAmount())
                .category(category)
                .type(type)
                .note(expenseData.getNote())
                .spentAt(expenseData.getSpentAt())
                .createdAt(expenseData.getCreatedAt())
                .build();
    }
}
