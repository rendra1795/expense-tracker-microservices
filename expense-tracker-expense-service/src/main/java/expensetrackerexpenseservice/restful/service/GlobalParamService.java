package expensetrackerexpenseservice.restful.service;

import expensetrackerexpenseservice.restful.model.data.DefaultData;

public interface GlobalParamService {
    public DefaultData getDefaultDataBySlugAndCodeId(String slug, Long codeId);
}
