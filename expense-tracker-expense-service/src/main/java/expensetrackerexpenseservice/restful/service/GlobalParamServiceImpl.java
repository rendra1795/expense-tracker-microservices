package expensetrackerexpenseservice.restful.service;

import expensetrackerexpenseservice.restful.entity.GlobalParam;
import expensetrackerexpenseservice.restful.model.data.DefaultData;
import expensetrackerexpenseservice.restful.repository.GlobalParamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class GlobalParamServiceImpl implements GlobalParamService {
    @Autowired
    private GlobalParamRepository globalParamRepository;

    @Override
    public DefaultData getDefaultDataBySlugAndCodeId(String slug, Long codeId) {
        GlobalParam globalParam = globalParamRepository.findBySlugAndCodeId(slug, codeId).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "Global param not found")
        );

        return DefaultData.builder()
                .id(globalParam.getCodeId())
                .name(globalParam.getDescription())
                .build();
    }
}
