package expensetrackerexpenseservice.restful.service;

import expensetrackerexpenseservice.restful.model.data.ExpenseDetailData;
import expensetrackerexpenseservice.restful.model.request.CreateExpenseDataRequest;
import expensetrackerexpenseservice.restful.model.request.ExpenseQueryParams;
import expensetrackerexpenseservice.restful.model.request.UpdateExpenseDataRequest;
import expensetrackerexpenseservice.restful.model.response.MonthlyExpenseReportResponse;
import expensetrackerexpenseservice.restful.model.response.PaginationBaseResponse;

public interface ExpenseDataService {
    public ExpenseDetailData createExpense(CreateExpenseDataRequest request, Long actorId);
    public ExpenseDetailData updateExpense(UpdateExpenseDataRequest request, Long expenseId, Long actorId);

    public PaginationBaseResponse<ExpenseDetailData> getAllExpense(ExpenseQueryParams param, Long userId);

    public MonthlyExpenseReportResponse getMonthlyReport(Integer month, Integer year, Long userId);

    public void deleteExpense(Long expenseId);
}
