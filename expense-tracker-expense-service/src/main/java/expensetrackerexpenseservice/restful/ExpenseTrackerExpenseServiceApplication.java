package expensetrackerexpenseservice.restful;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExpenseTrackerExpenseServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExpenseTrackerExpenseServiceApplication.class, args);
	}

}
