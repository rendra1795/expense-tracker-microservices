package expensetrackerexpenseservice.restful.model.data.query;

public interface MostExpenseProjection {
    Integer getData();
    Double getTotalSpent();
    Integer getDataCount();
}
