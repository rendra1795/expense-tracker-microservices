package expensetrackerexpenseservice.restful.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MostExpenseData {
    @JsonProperty(value = "totalAmount")
    private Double totalAmount;

    @JsonProperty(value = "count")
    private Integer count;

    @JsonProperty(value = "data")
    private DefaultData data;
}
