package expensetrackerexpenseservice.restful.model.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class ExpenseDetailData {
    @JsonProperty("id")
    private Long id;

    @JsonProperty(value = "amount")
    private Double amount;

    @JsonProperty(value = "category")
    private DefaultData category;

    @JsonProperty(value = "type")
    private DefaultData type;

    @JsonProperty(value = "note")
    private String note;

    @JsonProperty(value = "spentAt")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
    private Date spentAt;

    @JsonProperty(value = "createdAt")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
    private Date createdAt;
}
