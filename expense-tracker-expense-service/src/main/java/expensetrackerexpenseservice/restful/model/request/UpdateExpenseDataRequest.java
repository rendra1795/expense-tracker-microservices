package expensetrackerexpenseservice.restful.model.request;

import expensetrackerexpenseservice.restful.model.data.DefaultData;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
public class UpdateExpenseDataRequest {
    @NotNull
    @Min(value = 0)
    private Double amount;

    @NotNull
    private DefaultData category;

    @NotNull
    private DefaultData type;

    private String note;

    private Date spentAt;
}
