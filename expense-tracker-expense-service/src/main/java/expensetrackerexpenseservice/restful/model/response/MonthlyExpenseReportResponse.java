package expensetrackerexpenseservice.restful.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import expensetrackerexpenseservice.restful.model.data.DefaultData;
import expensetrackerexpenseservice.restful.model.data.MostExpenseData;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MonthlyExpenseReportResponse {
    @JsonProperty(value = "year")
    private Integer year;

    @JsonProperty(value = "month")
    private DefaultData month;

    @JsonProperty(value = "totalExpense")
    private Double totalExpense;

    @JsonProperty(value = "mostUsedCategory")
    private MostExpenseData mostUsedCategory;

    @JsonProperty(value = "mostExpenseCategory")
    private MostExpenseData mostExpenseCategory;

    @JsonProperty(value = "mostUsedMethod")
    private MostExpenseData mostUsedMethod;

    @JsonProperty(value = "mostExpenseMethod")
    private MostExpenseData mostExpenseMethod;
}
