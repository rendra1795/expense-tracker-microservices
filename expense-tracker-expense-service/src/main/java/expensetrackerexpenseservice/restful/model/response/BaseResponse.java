package expensetrackerexpenseservice.restful.model.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BaseResponse<T> {
    private T data;

    private String error;

    private int status;
}
