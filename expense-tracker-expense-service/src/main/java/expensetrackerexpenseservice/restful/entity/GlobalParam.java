package expensetrackerexpenseservice.restful.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "mr_global_param")
public class GlobalParam {
    @Id
    @Column(name = "mgp_id")
    private Long id;

    @Column(name = "mgp_slug")
    private String slug;

    @Column(name = "mgp_code_id")
    private Long codeId;

    @Column(name = "mgp_description")
    private String description;
}
