package expensetrackerexpenseservice.restful.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import java.util.Date;

@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@Entity
@Table(name = "expense_data")
public class ExpenseData {
    @Id
    @Column(name = "ed_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ed_user_id")
    private Long userId;

    @Column(name = "ed_category")
    private Long category;

    @Column(name = "ed_type")
    private Long type;

    @Column(name = "ed_amount")
    private Double amount;

    @Column(name = "ed_note")
    private String note;

    @Column(name = "ed_spent_at")
    @Temporal(TemporalType.DATE)
    private Date spentAt;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ed_created_at")
    private Date createdAt;

    @Column(name = "cd_created_by")
    private Long createdBy;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cd_updated_at")
    private Date updatedAt;

    @Column(name = "cd_updated_by")
    private Long updatedBy;
}
