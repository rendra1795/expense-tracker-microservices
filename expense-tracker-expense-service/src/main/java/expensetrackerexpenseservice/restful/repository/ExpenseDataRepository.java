package expensetrackerexpenseservice.restful.repository;

import expensetrackerexpenseservice.restful.entity.ExpenseData;
import expensetrackerexpenseservice.restful.model.data.query.MostExpenseProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ExpenseDataRepository extends JpaRepository<ExpenseData, Long> {
    @Query(
        value = "SELECT * FROM expense_data " +
                "WHERE ed_user_id = :userId " +
                "AND (:category IS NULL OR ed_category = :category) " +
                "AND (:type IS NULL OR ed_type = :type) " +
                "AND (:startDate IS NULL OR ed_spent_at >= :startDate) " +
                "AND (:endDate IS NULL OR ed_spent_at <= :endDate) ",
        countQuery = "SELECT COUNT(*) FROM expense_data " +
                "WHERE ed_user_id = :userId " +
                "AND (:category IS NULL OR ed_category = :category) " +
                "AND (:type IS NULL OR ed_type = :type) " +
                "AND (:startDate IS NULL OR ed_spent_at >= :startDate) " +
                "AND (:endDate IS NULL OR ed_spent_at <= :endDate) ",
        nativeQuery = true
    )
    public Page<ExpenseData> getAllExpensesWithFilter(
        @Param("userId") Long userId,
        @Param("category") Integer category,
        @Param("type") Integer type,
        @Param("startDate") Date startDate,
        @Param("endDate") Date endDate,
        Pageable pageable
    );

    @Query(
        value = "SELECT * FROM expense_data " +
                "WHERE ed_user_id = :userId " +
                "AND MONTH(ed_spent_at) = :month " +
                "AND YEAR(ed_spent_at) = :year",
        nativeQuery = true
    )
    public List<ExpenseData> getMonthlyExpenses(
        @Param("userId") Long userId,
        @Param("month") Integer month,
        @Param("year") Integer year
    );

    @Query(
        value = "SELECT ed_category AS data, SUM(ed_amount) AS totalSpent, COUNT(ed_category) AS dataCount " +
                "FROM expense_data " +
                "WHERE ed_user_id = :userId " +
                "AND MONTH(ed_spent_at) = :month " +
                "AND YEAR(ed_spent_at) = :year " +
                "GROUP BY ed_category " +
                "ORDER BY SUM(ed_amount) DESC " +
                "LIMIT 1",
        nativeQuery = true
    )
    public MostExpenseProjection getMostExpenseCategory(
            @Param("userId") Long userId,
            @Param("month") Integer month,
            @Param("year") Integer year
    );

    @Query(
        value = "SELECT ed_category AS data, SUM(ed_amount) AS totalSpent, COUNT(ed_category) AS dataCount " +
                "FROM expense_data " +
                "WHERE ed_user_id = :userId " +
                "AND MONTH(ed_spent_at) = :month " +
                "AND YEAR(ed_spent_at) = :year " +
                "GROUP BY ed_category " +
                "ORDER BY COUNT(ed_amount) DESC " +
                "LIMIT 1",
        nativeQuery = true
    )
    public MostExpenseProjection getMostUsedCategory(
            @Param("userId") Long userId,
            @Param("month") Integer month,
            @Param("year") Integer year
    );

    @Query(
        value = "SELECT ed_type AS data, SUM(ed_amount) AS totalSpent, COUNT(ed_type) AS dataCount " +
                "FROM expense_data " +
                "WHERE ed_user_id = :userId " +
                "AND MONTH(ed_spent_at) = :month " +
                "AND YEAR(ed_spent_at) = :year " +
                "GROUP BY ed_type " +
                "ORDER BY SUM(ed_amount) DESC " +
                "LIMIT 1",
        nativeQuery = true
    )
    public MostExpenseProjection getMostExpenseMethod(
            @Param("userId") Long userId,
            @Param("month") Integer month,
            @Param("year") Integer year
    );

    @Query(
        value = "SELECT ed_type AS data, SUM(ed_amount) AS totalSpent, COUNT(ed_type) AS dataCount " +
                "FROM expense_data " +
                "WHERE ed_user_id = :userId " +
                "AND MONTH(ed_spent_at) = :month " +
                "AND YEAR(ed_spent_at) = :year " +
                "GROUP BY ed_type " +
                "ORDER BY COUNT(ed_type) DESC " +
                "LIMIT 1",
        nativeQuery = true
    )
    public MostExpenseProjection getMostUsedMethod(
            @Param("userId") Long userId,
            @Param("month") Integer month,
            @Param("year") Integer year
    );
}
