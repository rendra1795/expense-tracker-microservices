package expensetrackerexpenseservice.restful.controller;

import expensetrackerexpenseservice.restful.model.data.ExpenseDetailData;
import expensetrackerexpenseservice.restful.model.data.UserData;
import expensetrackerexpenseservice.restful.model.request.CreateExpenseDataRequest;
import expensetrackerexpenseservice.restful.model.request.ExpenseQueryParams;
import expensetrackerexpenseservice.restful.model.request.UpdateExpenseDataRequest;
import expensetrackerexpenseservice.restful.model.response.BaseResponse;
import expensetrackerexpenseservice.restful.model.response.MonthlyExpenseReportResponse;
import expensetrackerexpenseservice.restful.model.response.PaginationBaseResponse;
import expensetrackerexpenseservice.restful.service.ExpenseDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(value = "api/expense", produces = MediaType.APPLICATION_JSON_VALUE)
public class ExpenseDataController {
    @Autowired
    private ExpenseDataService expenseDataService;

    @PostMapping
    private BaseResponse<ExpenseDetailData> createExpense(UserData userData, @RequestBody CreateExpenseDataRequest request) {
        return BaseResponse.<ExpenseDetailData>builder()
                .data(expenseDataService.createExpense(request, userData.getId()))
                .status(HttpStatus.OK.value())
                .build();
    }

    @PutMapping("/{expenseId}")
    private BaseResponse<ExpenseDetailData> updateExpense(
            UserData userData,
            @RequestBody UpdateExpenseDataRequest request,
            @PathVariable Long expenseId
    ) {
        return BaseResponse.<ExpenseDetailData>builder()
                .data(expenseDataService.updateExpense(request, expenseId, userData.getId()))
                .status(HttpStatus.OK.value())
                .build();
    }

    @GetMapping
    private PaginationBaseResponse<ExpenseDetailData> getAllExpense(
            UserData userData, @ModelAttribute ExpenseQueryParams params
    ) {
        if (params.getPage() == null) {
            params.setPage(1);
        }
        if (params.getLimit() == null) {
            params.setLimit(10);
        }
        return expenseDataService.getAllExpense(params, userData.getId());
    }

    @GetMapping("/monthly-report")
    private BaseResponse<MonthlyExpenseReportResponse> getMonthlyReport(
            UserData userData,
            @RequestParam(value = "month", required = false) Integer month,
            @RequestParam(value = "year", required = false) Integer year
    ) {
        if (month == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "month required");
        }
        if (year == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "year required");
        }

        return BaseResponse.<MonthlyExpenseReportResponse>builder()
                .data(expenseDataService.getMonthlyReport(month, year, userData.getId()))
                .status(HttpStatus.OK.value())
                .build();
    }

    @DeleteMapping("/{expenseId}")
    private BaseResponse<String> createExpense(UserData userData, @PathVariable Long expenseId) {
        expenseDataService.deleteExpense(expenseId);
        return BaseResponse.<String>builder()
                .data("Delete expense successful")
                .status(HttpStatus.OK.value())
                .build();
    }
}
