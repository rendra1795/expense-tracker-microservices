package expensetrackerexpenseservice.restful.controller;


import expensetrackerexpenseservice.restful.model.response.BaseResponse;
import jakarta.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

@RestControllerAdvice
public class ExceptionController {
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<BaseResponse<String>> constraintViolation(ConstraintViolationException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                BaseResponse.<String>builder().error(exception.getMessage()).status(HttpStatus.BAD_REQUEST.value()).build()
        );
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<BaseResponse<String>> constraintViolation(ResponseStatusException exception) {
        return ResponseEntity.status(exception.getStatusCode()).body(
                BaseResponse.<String>builder().error(exception.getReason()).status(exception.getStatusCode().value()).build()
        );
    }
}