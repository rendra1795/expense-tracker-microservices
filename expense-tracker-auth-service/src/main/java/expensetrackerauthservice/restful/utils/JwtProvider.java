package expensetrackerauthservice.restful.utils;

import expensetrackerauthservice.restful.entity.UserData;
import expensetrackerauthservice.restful.model.data.AuthData;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class JwtProvider implements tokenProvider {
    @Value("${jwt.secret-key}")
    private String secretKey;

    @Override
    public String generateToken(UserData user) {
        long expiredMillis = System.currentTimeMillis() + (1000 * 60 * 60 * 2);
        Date expiredDate = new Date(expiredMillis);

        Map<String, Object> claims = new HashMap<>();
        claims.put("id", user.getId());
        claims.put("username", user.getUsername());
        claims.put("name", user.getName());

        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(expiredDate)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    @Override
    public AuthData validateToken(String token) {
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            Claims claimsBody = claims.getBody();

            return AuthData.builder()
                    .id(claimsBody.get("id", Long.class))
                    .username(claimsBody.get("username", String.class))
                    .name(claimsBody.get("name", String.class))
                    .token(token)
                    .expiredAt(claimsBody.getExpiration())
                    .build();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Authentication failed");
        }
    }
}
