package expensetrackerauthservice.restful.utils;

import expensetrackerauthservice.restful.entity.UserData;
import expensetrackerauthservice.restful.model.data.AuthData;

public interface tokenProvider {
    public String generateToken(UserData user);
    public AuthData validateToken(String token);
}
