package expensetrackerauthservice.restful.controller;

import expensetrackerauthservice.restful.model.data.AuthData;
import expensetrackerauthservice.restful.model.request.UserLoginRequest;
import expensetrackerauthservice.restful.model.request.UserRegisterRequest;
import expensetrackerauthservice.restful.model.response.BaseResponse;
import expensetrackerauthservice.restful.model.response.UserLoginResponse;
import expensetrackerauthservice.restful.model.response.UserRegisterResponse;
import expensetrackerauthservice.restful.service.UserDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/auth")
@RestController
public class UserDataController {
    @Autowired
    private UserDataService userDataService;

    @PostMapping(path = "/register", consumes = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<UserRegisterResponse> register(@RequestBody UserRegisterRequest request) {
        return BaseResponse.<UserRegisterResponse>builder()
                .data(userDataService.register(request))
                .status(HttpStatus.OK.value())
                .build();
    }

    @PostMapping(path = "/login", consumes = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<UserLoginResponse> login(@RequestBody UserLoginRequest request) {
        return BaseResponse.<UserLoginResponse>builder()
                .data(userDataService.login(request))
                .status(HttpStatus.OK.value())
                .build();
    }

    @GetMapping(path = "/validate")
    public BaseResponse<AuthData> validateAuth(@RequestHeader(HttpHeaders.AUTHORIZATION) String authStr) {
        String token = authStr.substring("Bearer ".length());
        return BaseResponse.<AuthData>builder()
                .data(userDataService.validateToken(token))
                .status(HttpStatus.OK.value())
                .build();
    }
}
