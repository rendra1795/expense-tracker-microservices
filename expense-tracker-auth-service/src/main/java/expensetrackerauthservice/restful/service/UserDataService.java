package expensetrackerauthservice.restful.service;

import expensetrackerauthservice.restful.model.data.AuthData;
import expensetrackerauthservice.restful.model.request.UserLoginRequest;
import expensetrackerauthservice.restful.model.request.UserRegisterRequest;
import expensetrackerauthservice.restful.model.response.UserLoginResponse;
import expensetrackerauthservice.restful.model.response.UserRegisterResponse;

public interface UserDataService {
    public UserRegisterResponse register(UserRegisterRequest request);

    public UserLoginResponse login(UserLoginRequest request);

    public AuthData validateToken(String token);
}
