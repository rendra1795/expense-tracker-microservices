package expensetrackerauthservice.restful.service;

import expensetrackerauthservice.restful.entity.UserData;
import expensetrackerauthservice.restful.model.data.AuthData;
import expensetrackerauthservice.restful.model.request.UserLoginRequest;
import expensetrackerauthservice.restful.model.request.UserRegisterRequest;
import expensetrackerauthservice.restful.model.response.UserLoginResponse;
import expensetrackerauthservice.restful.model.response.UserRegisterResponse;
import expensetrackerauthservice.restful.repository.UserDataRepository;
import expensetrackerauthservice.restful.utils.JwtProvider;
import jakarta.transaction.Transactional;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validator;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Set;

@Service
public class UserDataServiceImpl implements UserDataService {
    @Autowired
    private UserDataRepository userDataRepository;

    @Autowired
    private JwtProvider jwtProvider;

    @Autowired
    private Validator validator;

    @Override
    @Transactional(rollbackOn = Exception.class)
    public UserRegisterResponse register(UserRegisterRequest request) {
        validateRequest(request);

        if (userDataRepository.existsByUsername(request.getUsername())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Username already exists");
        }

        UserData user = new UserData();
        user.setUsername(request.getUsername());
        user.setPassword(BCrypt.hashpw(request.getPassword(), BCrypt.gensalt()));
        user.setName(request.getName());

        UserData savedUserData = userDataRepository.save(user);

        return UserRegisterResponse.builder()
                .username(savedUserData.getUsername())
                .name(savedUserData.getName())
                .build();
    }

    @Override
    public UserLoginResponse login(UserLoginRequest request) {
        validateRequest(request);

        UserData user = userDataRepository.findByUsername(request.getUsername())
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.BAD_REQUEST, "You have entered an invalid username/password")
                );

        if (BCrypt.checkpw(request.getPassword(), user.getPassword())) {
            String token = jwtProvider.generateToken(user);

            return UserLoginResponse.builder()
                    .id(user.getId())
                    .username(user.getUsername())
                    .name(user.getName())
                    .token(token)
                    .build();
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You have entered an invalid username/password");
        }
    }

    @Override
    public AuthData validateToken(String token) {
        return jwtProvider.validateToken(token);
    }

    private void validateRequest(Object request) {
        Set<ConstraintViolation<Object>> validation = validator.validate(request);
        if (!validation.isEmpty()) {
            throw new ConstraintViolationException(validation);
        }
    }
}
