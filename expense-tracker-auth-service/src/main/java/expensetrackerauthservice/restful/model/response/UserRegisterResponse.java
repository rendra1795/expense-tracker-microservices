package expensetrackerauthservice.restful.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserRegisterResponse {
    @JsonProperty(value = "username")
    private String username;

    @JsonProperty(value = "name")
    private String name;
}
