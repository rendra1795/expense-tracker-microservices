package expensetrackerauthservice.restful;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExpenseTrackerAuthServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExpenseTrackerAuthServiceApplication.class, args);
	}

}
