package expensetrackerauthservice.restful.service;

import expensetrackerauthservice.restful.entity.UserData;
import expensetrackerauthservice.restful.model.request.UserLoginRequest;
import expensetrackerauthservice.restful.model.request.UserRegisterRequest;
import expensetrackerauthservice.restful.repository.UserDataRepository;
import expensetrackerauthservice.restful.utils.JwtProvider;
import jakarta.validation.Validator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class UserDataServiceImplTest {
    @InjectMocks
    private UserDataServiceImpl userDataService;

    @Mock
    private UserDataRepository userDataRepository;

    @Mock
    private JwtProvider jwtProvider;

    @Mock
    private Validator validator;

    private UserRegisterRequest registerRequest = UserRegisterRequest.builder().build();
    private UserLoginRequest loginRequest = UserLoginRequest.builder().build();

    private Long userId = 1L;

    @Test
    public void registerUser_shouldThrow_whenUsernameAlreadyExists() {
        when(userDataRepository.existsByUsername(any())).thenReturn(true);

        Throwable exception = Assertions.assertThrows(
                ResponseStatusException.class, (() -> userDataService.register(registerRequest))
        );

        Assertions.assertEquals(HttpStatus.BAD_REQUEST + " \"Username already exists\"", exception.getMessage());
    }

    @Test
    public void registerUser_shouldCall_validator_andUserDataRepository() {
        UserData userData = new UserData();
        when(userDataRepository.existsByUsername(any())).thenReturn(false);
        when(userDataRepository.save(any())).thenReturn(userData);

        userDataService.register(registerRequest);

        Mockito.verify(validator, Mockito.times(1)).validate(any());
        Mockito.verify(userDataRepository, Mockito.times(1)).existsByUsername(any());
        Mockito.verify(userDataRepository, Mockito.times(1)).save(any());
    }

    @Test
    public void login_shouldThrow_whenUsernameDoesNotExists() {
        Throwable exception = Assertions.assertThrows(
                ResponseStatusException.class, (() -> userDataService.login(loginRequest))
        );

        Assertions.assertEquals(
                HttpStatus.BAD_REQUEST + " \"You have entered an invalid username/password\"",
                exception.getMessage()
        );
    }

    @Test
    public void validateToken_shouldCall_jwtProviderValidateToken() {
        userDataService.validateToken("token");

        verify(jwtProvider, times(1)).validateToken("token");
    }

}