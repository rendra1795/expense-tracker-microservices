CREATE TABLE user_data(
    ud_id int(11) NOT NULL AUTO_INCREMENT,
    ud_username varchar(255),
    ud_name varchar(255),
    ud_password varchar(255),
    ud_created_at datetime DEFAULT CURRENT_TIMESTAMP,
    ud_created_by int(11) DEFAULT NULL,
    PRIMARY KEY(ud_id)
);