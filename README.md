# expense-tracker-microservices



# Getting started

Expense Tracker is an app designed to help individuals manage their personal finances. Users can register and log in to the app, enabling them to create, update, and delete expense records. The app provides features for tracking and categorizing expenses, empowering users to stay organized and gain valuable insights into their spending habits. Additionally, Expense Tracker offers the convenience of generating monthly expense reports, allowing users to analyze and optimize their financial decisions.

**Prerequisite:**
- Docker
- Docker Compose


**Getting started:**
1. Clone this repository.
2. Go to terminal and go to this repository.
3. Make sure your local device installed with Docker and Docker Compose.
4. Run `docker-compose up`
5. Wait for a while to setup the microservices app, before start to use the app.

**Note:**

Authentication API `baseUrl`: `http:/localhost:8080/`

Expense API `baseUrl`: `http:/localhost:8081/`

Run unit test command: `mvn test`

# **Documentation**
# Authentication API Endpoints:

### - [**POST**] Register: `{baseUrl}/api/auth/register`

**Request Body:**
```json
{
  "username": "johndoe",
  "password": "password123",
  "name": "John Doe"
}
```

**Response Success:**
```json
{
    "data": {
        "username": "johndoe",
        "name": "John Doe"
    },
    "error": null,
    "status": 200
}
```

**Response Failed [400]:**
```json
{
    "data": null,
    "error": "username: size must be between 3 and 16",
    "status": 400
}
```

### - [**POST**] Login: `{baseUrl}/api/auth/login`

**Request Body:**
```json
{
  "username": "johndoe",
  "password": "password123"
}
```

**Response Success:**
```json
{
    "data": {
        "id": 1,
        "username": "johndoe",
        "name": "John Doe",
        "token": "eyJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiQWRtaW4iLCJpZCI6MSwidXNlcm5hbWUiOiJhZG1pbiIsImV4cCI6MTcwNDYyMTcwM30.vCenb_BeqYrcC0xr15uAF_RIYVnYnwCGyl3L2SFU8Mg"
    },
    "error": null,
    "status": 200
}
```

**Response Failed [400]:**
```json
{
    "data": null,
    "error": "You have entered an invalid username/password",
    "status": 400
}
```

### - [**GET**] Validate Token: `{baseUrl}/api/auth/validate`

**Request Header:**
```
Authorization: Bearer {token}
```

**Response Success:**
```json
{
    "data": {
        "id": 1,
        "username": "johndoe",
        "name": "John Doe",
        "token": "eyJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiQWRtaW4iLCJpZCI6MSwidXNlcm5hbWUiOiJhZG1pbiIsImV4cCI6MTcwNDYyMTcwM30.vCenb_BeqYrcC0xr15uAF_RIYVnYnwCGyl3L2SFU8Mg",
        "token_expired_at": "2024-01-07 17:01:43"
    },
    "error": null,
    "status": 200
}
```

**Response Failed [401]:**
```json
{
  "data": null,
  "error": "Authentication failed",
  "status": 401
}
```

# Expense API Endpoints:

### - [**POST**] Create Expense: `{baseUrl}/api/expense`

**Request Header:**
```
Authorization: Bearer {token}
```

**Request Body:**
```json
{
  "amount": 60000.0,
  "category": {
      "id": 1, // Can not be null (identifier)
      "name": null // Can be null
  },
  "type": {
      "id": 6, // Can not be null (identifier)
      "name": null // Can be null
  },
  "note": "Grabfood - Chatime",
  "spentAt": "2024-01-05"
}
```

**Response Success:**
```json
{
  "data": {
      "id": 1,
      "amount": 60000.0,
      "category": {
          "id": 1,
          "name": "Food"
      },
      "type": {
          "id": 6,
          "name": "QRIS"
      },
      "note": "Grabfood - Chatime",
      "spentAt": "2024-01-05",
      "createdAt": "2024-01-07 15:03:51"
  },
  "error": null,
  "status": 200
}
```

**Response Failed [400]:**
```json
{
    "data": null,
    "error": "amount: must not be null",
    "status": 400
}
```

**Response Failed [401]:**
```json
{
    "data": null,
    "error": "Authentication failed",
    "status": 401
}
```

### - [**PUT**] Update Expense: `{baseUrl}/api/expense/{expenseId}`

**Request Header:**
```
Authorization: Bearer {token}
```

**Request Body:**
```json
{
  "amount": 49500.0,
  "category": {
      "id": 1, // Can not be null (identifier)
      "name": null // Can be null
  },
  "type": {
      "id": 5, // Can not be null (identifier)
      "name": null // Can be null
  },
  "note": "Go Food - Chatime",
  "spentAt": "2024-01-06"
}
```

**Response Success:**
```json
{
    "data": {
        "id": 1,
        "amount": 49500,
        "category": {
            "id": 1,
            "name": "Food"
        },
        "type": {
            "id": 5,
            "name": "eWallet"
        },
        "note": "Go Food - Chatime",
        "spentAt": "2024-01-06",
        "createdAt": "2024-01-07 15:02:10"
    },
    "error": null,
    "status": 200
}
```

**Response Failed [400]:**
```json
{
    "data": null,
    "error": "amount: must not be null",
    "status": 400
}
```

**Response Failed [401]:**
```json
{
    "data": null,
    "error": "Authentication failed",
    "status": 401
}
```

### - [**DELETE**] Delete Expense: `{baseUrl}/api/expense/{expenseId}`

**Request Header:**
```
Authorization: Bearer {token}
```

**Request Param:**

- `expenseId`: Id of expense data

**Response Success:**
```json
{
    "data": "Delete expense successful",
    "error": null,
    "status": 200
}
```

**Response Failed [401]:**
```json
{
    "data": null,
    "error": "Authentication failed",
    "status": 401
}
```

### - [**GET**] Get All Expense List + Filter: `{baseUrl}/api/expense`

Note: This endpoint has a feature to filter partially by category, type, or/and range of date expense spent only.

**Request Header:**
```
Authorization: Bearer {token}
```

****Request Param:****

- `page`: Requested page [Default = 1]
- `limit`: Limit date per-page [Default = 10]
- `categoryId`: Id of specific category
- `typeId`: Id of specific payment method type
- `startDate`: Start date of range the expense spent at
- `endDate`: End date of range the expense spent at


**Response Success:**
```json
{
    "currentPage": 1,
    "totalPage": 2,
    "totalData": 20,
    "pagingData": [
        {
            "id": 1,
            "amount": 3500,
            "category": {
                "id": 2,
                "name": "Transportation"
            },
            "type": {
                "id": 5,
                "name": "eWallet"
            },
            "note": "Transjakarta",
            "spentAt": "2024-01-03",
            "createdAt": "2024-01-06 20:41:09"
        },
        {
            "id": 2,
            "amount": 13000,
            "category": {
                "id": 2,
                "name": "Transportation"
            },
            "type": {
                "id": 5,
                "name": "eWallet"
            },
            "note": "Gojek",
            "spentAt": "2024-01-03",
            "createdAt": "2024-01-06 20:41:30"
        },
        {
            "id": 3,
            "amount": 25000,
            "category": {
                "id": 1,
                "name": "Food"
            },
            "type": {
                "id": 6,
                "name": "QRIS"
            },
            "note": "Ayam Geprek",
            "spentAt": "2024-01-03",
            "createdAt": "2024-01-06 20:41:47"
        },
        {
            "id": 4,
            "amount": 15000,
            "category": {
                "id": 1,
                "name": "Food"
            },
            "type": {
                "id": 6,
                "name": "QRIS"
            },
            "note": "Kopi Susu Family Mart",
            "spentAt": "2024-01-03",
            "createdAt": "2024-01-06 20:42:03"
        },
        {
            "id": 5,
            "amount": 40000,
            "category": {
                "id": 3,
                "name": "Entertainment"
            },
            "type": {
                "id": 6,
                "name": "QRIS"
            },
            "note": "Bioskop XXI",
            "spentAt": "2024-01-05",
            "createdAt": "2024-01-06 20:42:57"
        },
        {
            "id": 6,
            "amount": 525000,
            "category": {
                "id": 8,
                "name": "Grocery"
            },
            "type": {
                "id": 3,
                "name": "Debit Card"
            },
            "note": "Naga Swalayan",
            "spentAt": "2024-01-06",
            "createdAt": "2024-01-06 20:43:20"
        },
        {
            "id": 7,
            "amount": 2000000,
            "category": {
                "id": 4,
                "name": "Savings"
            },
            "type": {
                "id": 3,
                "name": "Debit Card"
            },
            "note": "Auto deposit reksa dana",
            "spentAt": "2024-01-01",
            "createdAt": "2024-01-06 20:44:12"
        },
        {
            "id": 8,
            "amount": 3000000,
            "category": {
                "id": 10,
                "name": "Travel"
            },
            "type": {
                "id": 2,
                "name": "Bank Transfer"
            },
            "note": "Sewa vila di puncak",
            "spentAt": "2023-12-31",
            "createdAt": "2024-01-06 20:44:58"
        },
        {
            "id": 9,
            "amount": 60000,
            "category": {
                "id": 1,
                "name": "Food"
            },
            "type": {
                "id": 2,
                "name": "Bank Transfer"
            },
            "note": "Jagung Bakar",
            "spentAt": "2023-12-31",
            "createdAt": "2024-01-06 20:45:19"
        },
        {
            "id": 10,
            "amount": 350000,
            "category": {
                "id": 2,
                "name": "Transportation"
            },
            "type": {
                "id": 1,
                "name": "Cash"
            },
            "note": "Bensin Mobil",
            "spentAt": "2023-12-31",
            "createdAt": "2024-01-06 20:47:21"
        }
    ]
}
```

**Response Failed [401]:**
```json
{
    "data": null,
    "error": "Authentication failed",
    "status": 401
}
```


### - [**GET**] Get All Expense List + Filter: `{baseUrl}/api/expense/monthly-report`

**Request Header:**
```
Authorization: Bearer {token}
```

****Request Param:****

- `month`: Month of expense
- `year`: Year of expense


**Response Success:**
```json
{
    "data": {
        "year": 2024,
        "month": {
            "id": 1,
            "name": "JANUARY"
        },
        "totalExpense": 3043500,
        "mostUsedCategory": {
            "totalAmount": 112000,
            "count": 3,
            "data": {
                "id": 1,
                "name": "Food"
            }
        },
        "mostExpenseCategory": {
            "totalAmount": 2000000,
            "count": 1,
            "data": {
                "id": 4,
                "name": "Savings"
            }
        },
        "mostUsedMethod": {
            "totalAmount": 430000,
            "count": 4,
            "data": {
                "id": 6,
                "name": "QRIS"
            }
        },
        "mostExpenseMethod": {
            "totalAmount": 2525000,
            "count": 2,
            "data": {
                "id": 3,
                "name": "Debit Card"
            }
        }
    },
    "error": null,
    "status": 200
}
```

**Response Failed [400]:**
```json
{
    "data": null,
    "error": "month required",
    "status": 400
}
```

**Response Failed [401]:**
```json
{
    "data": null,
    "error": "Authentication failed",
    "status": 401
}
```